var a = 0;
var b = 0;
var c = 0;

$(document).ready(function(){
	$("#isiMenu").hide();
	$("#isiSearch").hide();
	
	$("#FullMenu").click(function(){
		$("#isiMenu").toggle(100);
  		bgtoggle();
	});	

	$("#search").click(function(){
		$("#isiSearch").show();
		bg2toggle();
	});	

	$("#back").click(function(){
		$("#isiSearch").hide();
		bg2toggle();
	});	

	$("#badan").click(function(){
		$("#isiMenu").hide();
		$("#isiSearch").hide();
		document.getElementById("badan").style.width = "0";
		document.getElementById("badan").style.height = "0";
		document.getElementById("mySidenav").style.width = "0px";
		document.getElementById("menuSearch").style.width = "0px";
		a = 0;
		b = 0;
		c = 0;
	});	
});

function bgtoggle() {
	if (a == 0) {
		document.getElementById("badan").style.width = "100%";
		document.getElementById("badan").style.height = "100%";
		document.getElementById("mySidenav").style.width = "18%";
		a = 1;
		c = 1;
	} else if (a == 1) {
		document.getElementById("badan").style.width = "0";
		document.getElementById("badan").style.height = "0";
		document.getElementById("mySidenav").style.width = "0px";
		a = 0;
		c = 0;
	}
}

function bg2toggle() {
	if (b == 0) {
		document.getElementById("badan").style.width = "100%";
		document.getElementById("badan").style.height = "100%";
		document.getElementById("menuSearch").style.width = "40%";
		b = 1;
	} else if (b == 1) {
		if(c != 1){
			document.getElementById("badan").style.width = "0";
			document.getElementById("badan").style.height = "0";
			c = 0;
		}
		document.getElementById("menuSearch").style.width = "0px";
		b = 0;
	}
}

function biasa() {
	document.getElementById("actived1").style.background = "transparent";
	document.getElementById("actived2").style.background = "transparent";
	document.getElementById("actived3").style.background = "transparent";
	document.getElementById("actived4").style.background = "transparent";
	document.getElementById("actived5").style.background = "transparent";
}

function hoverbtn(arg) {
	var klass = "actived";
	var ini = klass + arg;

	biasa();
	document.getElementById(ini).style.background = "#2025282B";
}